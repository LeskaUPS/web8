$(document).ready(function(){
    PopUpHide();
});
function PopUpShow(){
    $("#popup1").show();
    history.pushState(null,'form','./forma');
    $("#email").val(localStorage.getItem('email'));
    $("#name").val(localStorage.getItem('name'));
    $("#phone").val(localStorage.getItem('phone'));
    $("#message").val(localStorage.getItem('message'));

}
function PopUpHide(){
    $("#popup1").hide();
    history.replaceState({id: null}, 'Default state', './');
}
window.addEventListener('popstate', function() {
	PopUpHide();
});